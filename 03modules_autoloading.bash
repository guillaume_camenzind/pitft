#!/bin/bash

sudo bash -c 'echo "spi-bcm2708" >> /etc/modules;echo "fbtft_device" >> /etc/modules;echo "options fbtft_device name=adafruitrt28 rotate=90 frequency=32000000" >> /etc/modprobe.d/adafruit.conf'

echo "export FRAMEBUFFER=/dev/fb1" >> ~/.profile
sudo reboot
